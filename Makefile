TEX     = pdflatex
BIBTEX  = bibtex

TEXFLAGS    = -halt-on-error -interaction=batchmode
BIBTEXFLAGS =

_rm = rm -f

TEXFILE = raman3thround
OTHER_FILES := preamble.tex
BIB_FILES := $(shell find `pwd` -name '*.bib' 2>/dev/null)
TABLE_FILES := $(shell find `pwd`/tables -name '*.tex' 2>/dev/null)

default:
	@echo "\n============= IMAGE FILES ============="
	$(MAKE) images -i
	@echo "=======================================\n"
	@echo "\n============== MAIN FILE =============="
	$(MAKE) ${TEXFILE}.pdf
	@echo "=======================================\n"

.SUFFIXES: .tex

$(TEXFILE).pdf: $(TEXFILE).tex $(OTHER_FILES) ${BIB_FILES} ${TABLE_FILES}
	$(MAKE) clean
	$(TEX) $(TEXFLAGS) $<
	$(eval name := $(basename $<))
	if grep -s "There were undefined references" $(name).log; then \
	  $(BIBTEX) $(BIBTEXFLAGS) $(name); \
	fi
	$(TEX) $(TEXFLAGS) --synctex=1 $<
	count=1 ;\
	while grep -s "Rerun to get" $(name).log && [ $$count -le 3 ] ; do \
	  $(TEX) $(TEXFLAGS) --synctex=1 $< ;\
	  count=`expr $$count + 1` ;\
	done
	@echo ""

images:
	cd tikz && $(MAKE)

.PHONY: clean 

clean:
	$(_rm) *.aux *.idx *.blg *.bbl *.glo *.bz2 *.toc *.lof *.lot \
	      *.syx *.abx *.lab *.ilg *.los *.ind *.gls *.out *~ \
	      *.log *.soc *.spl 
